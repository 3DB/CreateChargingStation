"""
Creates a file containing all relevant information of a specific charging station.

:author: Thogs
:date: 25.8.18
"""

import json
import os.path

# <Settings>
station_dir = "Stations"
# </Settings>

def get_int(text: str) -> int:
    while True:
        try:
            return int(input(text))
        except:
            print("[!] Not a number")

def get_float(text: str) -> float:
    while True:
        try:
            return float(input(text))
        except:
            print("[!] Not a float!")

json_dict = {}

json_dict["station_id"] = get_int("[?] Station ID: ")
json_dict["authority_id"] = get_int("[?] Authority ID: ")
json_dict["station_name"] = input("[?] Station Name: ")
json_dict["ampacity"] = get_float("[?] Ampacity: ")
json_dict["plug_type"] = input("[?] Plug Type: ")
json_dict["price_per_kwh"] = get_float("[?] Price per kWh: ")
json_dict["additions"] = get_float("[?] Additional fixed price: ")
json_dict["usage_state"] = "open"

json_str = json.dumps(json_dict)

file_name = os.path.join(station_dir, str(json_dict["station_id"]))
if not os.path.isdir(station_dir):
    os.mkdir(station_dir)

with open(file_name, "w") as f:
    f.write(json_str)

print("\n[*] Saved " + file_name + "!")